export default
  APP: =>
    APP = new Mongo.Collection 'app'

    if APP.find().count() is 0
      APP.insert
        id: 'APP'
        config:
          'languages': [ 'en' ]

    if not Meteor.users.findOne({ username: 'admin.picnicpass' })
      Accounts.createUser
        username: 'admin.picnicpass'
        email: 'hello@picnicpass.com'
        password: 'Hellopicnic1'

      Roles.addUsersToRoles Meteor.users.findOne({ username: 'admin.picnicpass' })._id, ['admin'], Roles.GLOBAL_GROUP

    sessions = Sessions.find({}).fetch()

    sessions.map (session) =>
      if not session.landing_option
        Sessions.update({ _id: session._id }, { $set: { landing_option: 1 } })

    APP
