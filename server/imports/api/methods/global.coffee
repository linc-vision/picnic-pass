export default
  getInitConfig: (data) ->
    future = new Future()

    App = APP.findOne()

    data = if data then data else ''

    res =
      config:
        welcomeMessage: "Welcome!"
        language: App.config.languages[0]
        landing_option: Math.floor(Math.random() * 3) + 1

    responseWithoutRef = =>
      future.return
        code: 'S'
        message: ''
        data:
          default: res

    responseWithRef = (follow) =>
      future.return
        code: 'S'
        message: ''
        data:
          default: res
          follow: follow

    responseWithCTA = (data) =>
      future.return
        code: 'S'
        message: ''
        data:
          default: res
          cta: data.cta
          cta_email: data.cta_email

    if data.length > 0
      Meteor.call 'checkUrlQuery', data, (err, res) =>
        if err or res.code is 'F'
          responseWithoutRef()
        else
          if res.message is 'CTA'
            responseWithCTA res.data
          else
            responseWithRef res.data
    else
      responseWithoutRef()

    future.wait()



  checkUrlQuery: (query) ->
    future = new Future()

    query = queryString.parse query

    if query.ref
      ref = Sessions.findOne({ referral: query.ref })

      if ref
        id = random 'Aa0', 16

        future.return
          code: 'S'
          message: ''
          data:
            id: id
            refTo: query.ref
      else
        future.return
          code: 'F'
          message: ''
    else if query.cta
      future.return
        code: 'S'
        message: 'CTA'
        data:
          cta: yes
          cta_email: query.cta
    else
      future.return
        code: 'F'
        message: ''

    future.wait()



  addPhoneNumber: (data) ->
    future = new Future()

    phone_number = data.number
    ref_from = data.refFrom
    cta = data.cta
    cta_email = data.cta_email
    landing_option = data.landing_option

    valid = no

    if phone_number.substring(0, 3) is '001'
      if phone_number.length is 13
        valid = yes
    else if phone_number.substring(0, 2) is '+1'
      if phone_number.length is 12
        valid = yes
    else if phone_number.substring(0, 1) is '1'
      if phone_number.length is 11
        valid = yes
    else
      if phone_number.length is 10
        valid = yes

    if valid
      if not Sessions.findOne({ phoneNumber: phone_number })
        ref_id = random 'Aa0', 16

        session =
          _id: ref_id
          sessionId: random 'Aa0', 16
          phoneNumber: phone_number
          referral: ref_id
          landing_option: landing_option
          peopleAhead: Sessions.find().count()

        if ref_from
          session.refFrom = ref_from

          Sessions.update({ referral: ref_from.refTo }, { $push: { 'refs': { id: ref_from.id } } })

          refNumberInQueue = Sessions.findOne({ referral: ref_from.refTo }).peopleAhead
          previousSubscriberId = Sessions.findOne({ peopleAhead: refNumberInQueue - 1 })._id
          previousSubscriberNumberInQueue = Sessions.findOne({ peopleAhead: refNumberInQueue - 1 }).peopleAhead

          Sessions.update({ referral: ref_from.refTo }, { $set: { 'peopleAhead': refNumberInQueue - 1 } })
          Sessions.update({ _id: previousSubscriberId }, { $set: { 'peopleAhead': previousSubscriberNumberInQueue + 1 } })
        if cta then session.cta = cta
        if cta_email then session.cta_email = cta_email

        Sessions.insert session

        future.return
          code: 'S'
          message: 'The phone number successfully added'
          data: session
      else
        future.return
          code: 'F'
          message: 'The phone number already exists'
    else
      future.return
        code: 'F'
        message: 'The phone number is invalid'

    future.wait()
