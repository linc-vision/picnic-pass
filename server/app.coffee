import '/server/lib/global_scope'

import init from './imports/operations/init'

import Sessions from '/server/imports/db/sessions'

import global from './imports/api/methods/global'



@Sessions = Sessions
@APP = init.APP()



Meteor.startup =>
  console.log "----------------------------------------------------------------------------"
  console.log "|                       Server started successfully                        |"
  console.log "----------------------------------------------------------------------------"



  Meteor.methods {
    global...
  }



  Meteor.publish 'sessions', =>
    if Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
      Sessions.find({}, { sort: { peopleAhead: 1 } })
    else
      []
