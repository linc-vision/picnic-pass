import { render } from 'react-dom'
import { Provider } from 'react-redux'
import redux from '/client/imports/redux/kit'
import { ConnectedRouter as Router, routerMiddleware } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

import '/client/lib/global_scope'

import App from './imports/kit'



history = createBrowserHistory()

if Meteor.isDevelopment
  @STORE = createStore redux(history), composeWithDevTools applyMiddleware routerMiddleware(history), thunk
else
  @STORE = createStore redux(history), applyMiddleware routerMiddleware(history), thunk



Meteor.startup ->
  render <Provider store={ STORE }>
      <Router history={ history } >
        <App/>
      </Router>
    </Provider>, document.getElementById 'root'
