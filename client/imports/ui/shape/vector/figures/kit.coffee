export default

  trapezoid: (color) =>
    <svg width="100%" height="100%" viewBox="0 0 598 98">
      <path d="M30.3781298,0 L0.661148655,105.5 L567.62187,105.5 L597.338851,0 L30.3781298,0 Z" id="Rectangle" fill={ color }></path>
    </svg>
