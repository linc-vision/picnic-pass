import './kit.styl'



SimpleLoading = class extends Component
  constructor: (props) ->
    super props



  componentDidMount: =>
    anime
      targets: ['#SimpleLoading > #icon > img']
      scale: [0, 1]
      rotate: [-120, 0]
      baseFrequency: 0
      duration: 800
      endDelay: 200
      loop: yes
      direction: 'alternate'
      easing: 'easeInOutQuint'
      loopComplete: (anim) =>
        if anim.reversed
          config = @props.app.config
          if config
            anim.pause()
            @props.app.setAppLoaded()

    @props.app.getInitConfig @props.location.search



  render: =>
    <div id='SimpleLoading' className={ classNames 'untouchable', 'cursor_loading', 'mobile': IS.mobile() }>
      <div id='icon'>
        <img src='/img/icons/logo.png' alt='Picnic Pass Logo'/>
      </div>
    </div>



export default outfit SimpleLoading
