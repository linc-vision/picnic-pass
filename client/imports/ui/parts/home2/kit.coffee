import './kit.styl'

import figures from '/client/imports/ui/shape/vector/figures/kit'

import { FacebookShareButton, TwitterShareButton, EmailShareButton, LinkedinShareButton } from 'react-share'

import copy from 'copy-to-clipboard'



Home2 = class extends Component
  constructor: (props) ->
    super props
    @state =
      page: 'landing'
      number: ''
      numberValid: null
      showError: no
      errorMessage: 'Please enter a valid number.'
      referralCopied: no



  componentDidMount: =>
    analytics.track 'Viewed Landing Page #2'
    anime
     targets: '#Home2'
     duration: 700
     opacity: [0, 1]
     scale: [0.6, 1]

    # @showThankYouPage()



  typeNumber: (e) =>
    if @state.showError then @hideError()
    @setState
      number: e.target.value.replace(/[^0-9-+()]+/g, '')
    , =>
      if @state.errorMessage isnt 'Please enter a valid number.'
        @setState
          errorMessage: 'Please enter a valid number.'

  validateNumber: (callback) =>
    phone_number = @state.number
    valid = no

    if phone_number.substring(0, 3) is '001'
      if phone_number.length is 13
        valid = yes
    else if phone_number.substring(0, 2) is '+1'
      if phone_number.length is 12
        valid = yes
    else if phone_number.substring(0, 1) is '1'
      if phone_number.length is 11
        valid = yes
    else
      if phone_number.length is 10
        valid = yes

    @setState
      numberValid: valid
    , =>
      if @state.numberValid is no then @showError()
      if typeof(callback) is 'function' then callback()

  showError: =>
    if not @state.showError
      @setState
        showError: yes

  hideError: =>
    if @state.showError
      @setState
        showError: no

  submit: =>
    @validateNumber =>
      if @state.numberValid
        data = {}
        data.number = @state.number
        data.refFrom = @props.app.status.user.refFrom
        data.cta = @props.app.status.user.cta
        data.cta_email = @props.app.status.user.cta_email
        data.landing_option = @props.app.config.landing_option

        analytics.track '(#2) User entered a phone number'

        Meteor.call 'addPhoneNumber', data, (err, res) =>
          if err
            console.log err
          else
            if res.code is 'F'
              if res.message is 'The phone number already exists'
                @setState
                  numberValid: no
                  errorMessage: 'The phone number already exists'
                , =>
                  if @state.numberValid is no then @showError()
                  if typeof(callback) is 'function' then callback()
              else
                @setState
                  numberValid: no
                , =>
                  if @state.numberValid is no then @showError()
                  if typeof(callback) is 'function' then callback()
            else
              @props.app.setSession res
              @showThankYouPage()

  showThankYouPage: =>
    if @state.page is 'landing'
      anime
        targets: '#Home2 > #landing'
        duration: 500
        opacity: [1, 0]
        scale: [1, 0.6]
        translateY: if IS.mobile() then [0, 0] else ['-50%', '-50%']
        complete: =>
          @setState
            page: 'thank_you'
          , =>
            anime
              targets: '#Home2 > #thank_you'
              duration: 0
              opacity: [0, 1]
              scale: [0.6, 1]
              translateY: if IS.mobile() then [0, 0] else ['-50%', '-50%']
            anime
              targets: '#Home2 > #thank_you > #center'
              duration: 700
              delay: 300
              opacity: [0, 1]
              scale: [0.6, 1]
              translateX: if IS.mobile() then [0, 0] else ['-75%', '-50%']
              translateY: if IS.mobile() then [0, 0] else ['-75%', '-50%']
            anime
              targets: '#Home2 > #thank_you > #left'
              duration: 700
              translateX: ['-50%', 0]
              translateY: if IS.mobile() then ['-50%', '-50%'] else [0, 0]
            anime
              targets: '#Home > #thank_you > #right'
              duration: 700
              translateX: ['50%', 0]
              translateY: if IS.mobile() then ['-50%', '-50%'] else [0, 0]

  copyReferral: =>

    analytics.track '(#2) User copied his referral link'

    copy @props.app.status.user.referralUrl
    setTimeout () =>
      copy @props.app.status.user.referralUrl
    , 300
    @setState
      referralCopied: yes
    , =>
      setTimeout () =>
        @setState
          referralCopied: no
      , 5000

  shareTrigger: (service) =>
    analytics.track "(#2) User shared his referral link via #{ service }"



  render: =>
    <div id='Home2' className={ classNames 'mobile': IS.mobile() }>
      <div id='tooper' className='untouchable'>
      {
        if not IS.mobile()
            <div id='pictures_1'>
              <img src='/img/pictures/bg_element_1.png'/>
            </div>
      }
        <div id='pictures_2'>
          <img src='/img/pictures/bg_element_2.png'/>
        </div>
      </div>
      <div id='header'>
        <div id='left'>
          <img src='/img/pictures/logo_white.png' alt='Picnic Pass Logo'/>
        </div>
        <div id='right'>
          <p>UC Davis</p>
        </div>
      </div>
      {
        if @state.page is 'landing'
          <div id='landing'>
            <div id='right'>
              <h1>Lunch for $8</h1>
              <h3>Be the first to get Picnic Pass at UC<br/> Davis and <strong>get your first month free</strong></h3>
              <div id='input'>
                <div id='icon' className='untouchable'></div>
                <input className={ classNames 'invalid': @state.numberValid is no } maxLength='16' type='text' name='phone' placeholder='Phone number' value={ @state.number } onChange={ @typeNumber } onBlur={ @validateNumber }/>
                <div id='error' className={ classNames 'cursor_pointer', 'untouchable': not @state.showError, 'hidden': not @state.showError } onClick={ @hideError }>
                  <p>{ @state.errorMessage }</p>
                </div>
              </div>
              <button className='cursor_pointer' onClick={ @submit }>Get Early Access</button>
            </div>
            <div id='left'>
              <div id='how_works'><h1>How Picnic Pass works</h1></div>
              <div id='list'>
                <div className='item' id='first'>
                  <div id='number'></div>
                  <h1>Choose your meal!</h1>
                  <p>Over 100 local restaurants offering lunch every day</p>
                </div>
                <div className='item' id='second'>
                  <div id='number'></div>
                  <h1>Skip the line!</h1>
                  <p>Skip the line-Picnic Pass covers the cost of lunch.</p>
                </div>
                <div className='item' id='third'>
                  <div id='number'></div>
                  <h1>Enjoy & Do it Again!</h1>
                  <p>Enjoy lunch from the best restaurants everyday.</p>
                </div>
              </div>
            </div>
          </div>
        else
          <div id='thank_you'>
            <div id='center'>
              <h3>Thank you!</h3>
              <p>We have added you mobile number to the signup queue.</p>
              <h4>{ "#{ @props.app.status.user.peopleAhead.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') } People ahead of you" }</h4>
              <div id='priority'>
                <h4>Interested in priority access?</h4>
                <p>Get early access by referring your friends. The more friends that joins, the sooner you’ll get access.</p>
                <div id='share'>
                  <FacebookShareButton url={ @props.app.status.user.referralUrl } quote={ 'Enjoy lunch from the best restaurants everyday.' } className='icon cursor_pointer'>
                    <div id='facebook' className='icon cursor_pointer' onClick={ @shareTrigger.bind this, 'Facebook' }></div>
                  </FacebookShareButton>
                  <TwitterShareButton url={ @props.app.status.user.referralUrl } title={ 'Enjoy lunch from the best restaurants everyday.' } className='icon cursor_pointer'>
                    <div id='twitter' className='icon cursor_pointer' onClick={ @shareTrigger.bind this, 'Twitter' }></div>
                  </TwitterShareButton>
                  <EmailShareButton url={ @props.app.status.user.referralUrl } subject={ 'Enjoy lunch from the best restaurants everyday.' } body={ 'I invite you to join Picnic Pass!' } className='icon cursor_pointer'>
                    <div id='email' className='icon cursor_pointer' onClick={ @shareTrigger.bind this, 'Email' }></div>
                  </EmailShareButton>
                  <LinkedinShareButton url={ @props.app.status.user.referralUrl } className='icon cursor_pointer'>
                    <div id='linked_in' className='icon cursor_pointer' onClick={ @shareTrigger.bind this, 'Linked In' }></div>
                  </LinkedinShareButton>
                </div>
                <p>Or share this unique link:</p>
                <p id='link' className={ classNames 'cursor_pointer': not @state.referralCopied, 'copied': @state.referralCopied } onClick={ if @state.referralCopied is no then @copyReferral }>{ if @state.referralCopied then 'Copied!' else @props.app.status.user.referralUrl }</p>
              </div>
            </div>
          </div>
      }
      <div id='footer'>
        <div id='socials'>
          <a href='https://instagram.com' target='_blank'><div id='instagram' className='icon cursor_pointer'></div></a>
          <a href='https://facebook.com' target='_blank'><div id='facebook' className='icon cursor_pointer'></div></a>
          <a href='https://twitter.com' target='_blank'><div id='twitter' className='icon cursor_pointer'></div></a>
        </div>
      </div>
      <div id='food' className='untouchable'>
        <div id='food_2'>
          <img src='/img/pictures/food_2.png'/>
        </div>
        <div id='food_3'>
          <img src='/img/pictures/food_3.png'/>
        </div>
        <div id='food_4'>
          <img src='/img/pictures/food_4.png'/>
        </div>
      </div>
    </div>



export default outfit Home2
