import './kit.styl'

import figures from '/client/imports/ui/shape/vector/figures/kit'



Admin = class extends Component
  constructor: (props) ->
    super props
    @state =
      username: ''
      password: ''
      valid: null
      showError: no
      errorMessage: 'Error'



  componentDidMount: =>
    anime
      targets: '#Admin > #content'
      duration: 500
      opacity: [0, 1]
      scale: [0.6, 1]
      translateY: ['-50%', '-50%']
    @showPage()



  showPage: =>
    anime
      targets: '#Admin > #content > #center'
      duration: 700
      opacity: [0, 1]
      scale: [0.6, 1]
      translateX: ['-75%', '-50%']
      translateY: ['-75%', '-50%']
    anime
      targets: '#Admin > #content > #left'
      duration: 700
      translateX: ['-50%', 0]
      translateY: [0, 0]
    anime
      targets: '#Admin > #content > #right'
      duration: 700
      translateX: ['50%', 0]
      translateY: [0, 0]

  hidePage: (callback) =>
    anime
      targets: '#Admin > #content > #center'
      duration: 700
      opacity: [1, 0]
      scale: [1, 0.6]
      translateX: ['-75%', '-50%']
      translateY: ['-75%', '-50%']
      complete: =>
        callback()
        @showPage()
    anime
      targets: '#Admin > #content > #left'
      duration: 700
      translateX: [0, '-50%']
      translateY: [0, 0]
    anime
      targets: '#Admin > #content > #right'
      duration: 700
      translateX: [0, '50%']
      translateY: [0, 0]

  typeUsername: (e) =>
    @setState
      username: e.target.value

  typePassword: (e) =>
    @setState
      password: e.target.value

  login: =>
    Meteor.loginWithPassword @state.username, @state.password, (err, res) =>
      if err
        @setState
          errorMessage: err.reason
          valid: no
        , =>
          @showError()
      else
        @hidePage =>
          @props.app.checkLogin()

  logout: =>
    @hidePage =>
      Meteor.logout (err, res) =>
        if not err
          @props.app.checkLogin()

  showError: =>
    if not @state.showError
      @setState
        showError: yes

  hideError: =>
    if @state.showError
      @setState
        showError: no



  render: =>
    <div id='Admin'>
      <div id='header'>
        <div id='left'>
          <img src='/img/pictures/logo.png' alt='Picnic Pass Logo'/>
        </div>
        <div id='center'>
          {
            if @props.app.status.user.logged
              <div id='layout'>
                <div id='subscribers'>
                  <p>subscribers</p>
                  <h3>{ @props.app.sessions.length }</h3>
                </div>
                <div className='landing'>
                  <p>Landing #1</p>
                  <h3>{ _.where(@props.app.sessions, { landing_option: 1 }).length }</h3>
                </div>
                <div className='landing'>
                  <p>Landing #2</p>
                  <h3 style={{ color: '#ED7375' }}>{ _.where(@props.app.sessions, { landing_option: 2 }).length }</h3>
                </div>
                <div className='landing'>
                  <p>Landing #3</p>
                  <h3 style={{ color: '#DD8F77' }}>{ _.where(@props.app.sessions, { landing_option: 3 }).length }</h3>
                </div>
              </div>
          }
        </div>
        <div id='right'>
          <p>UC Davis</p>
          {
            if @props.app.status.user.logged
              <p id='logout' className='cursor_pointer' onClick={ @logout }>log out</p>
          }
        </div>
      </div>
      <div id='content'>
        <div id='left'>
          <div id='list'>
            <div className='item' id='first'>
              <div id='layout'>{ figures.trapezoid '#8DD89D' }</div>
            </div>
            <div className='item' id='second'>
              <div id='layout'>{ figures.trapezoid '#F2D169' }</div>
            </div>
            <div className='item' id='third'>
              <div id='layout'>{ figures.trapezoid '#EC9D65' }</div>
            </div>
            <div id='food' className='untouchable'>
              <div id='food_1'>
                <img src='/img/pictures/food_1.png'/>
              </div>
              <div id='food_3'>
                <img src='/img/pictures/food_3.png'/>
              </div>
            </div>
          </div>
        </div>
        {
          if not @props.app.status.user.logged
            <div id='center' className='login'>
              <h3>Sign In</h3>
              <div id='form'>
                <input className={ classNames 'invalid': @state.valid is no } type='text' name='username' placeholder='Username' value={ @state.username } onChange={ @typeUsername }/>
                <input className={ classNames 'invalid': @state.valid is no } type='password' name='password' placeholder='Password' value={ @state.password } onChange={ @typePassword }/>
                <div id='error' className={ classNames 'cursor_pointer', 'untouchable': not @state.showError, 'hidden': not @state.showError } onClick={ @hideError }>
                  <p>{ @state.errorMessage }</p>
                </div>
                <button className='cursor_pointer' onClick={ @login }>Login</button>
              </div>
            </div>
          else
            <div id='center' className='admin'>
              <div id='list'>
                {
                  @props.app.sessions.map (object, n) =>
                    <div key={ n } className='object'>
                      <div id='left'>
                        <h3 className='selectable_text'>{ object.phoneNumber }</h3>
                        {
                          if object.cta
                            <div id='cta'>
                              <div id='icon'></div>
                              <p className='selectable_text'>{ if object.cta_email then "#{ object.cta_email } (CTA)" else "CTA" }</p>
                            </div>
                        }
                      </div>
                      <div id='right'>
                        {
                          if object.landing_option
                            <div id='landing_option'>
                              <p>Landing<br/>Option</p>
                              {
                                if object.landing_option is 2
                                  <h3 style={{ color: '#FFFFF' }}>{ "##{ object.landing_option }" }</h3>
                                else if object.landing_option is 3
                                  <h3 style={{ color: '#FFFFF' }}>{ "##{ object.landing_option }" }</h3>
                                else
                                  <h3 style={{ color: '#FFFFF' }}>{ "##{ object.landing_option }" }</h3>
                              }
                            </div>
                        }
                        <div id='ref_count'>
                          <p>Referrals<br/>Number</p>
                          <h3>{ if object.refs then object.refs.length else 0 }</h3>
                        </div>
                        <div id='number'>
                          <p>Queue<br/>Number</p>
                          <h3>{ object.peopleAhead + 1 }</h3>
                        </div>
                      </div>
                    </div>
                }
              </div>
            </div>
        }
        <div id='right'>
          <div id='list'>
            <div className='item' id='first'>
              <div id='layout'>{ figures.trapezoid '#8DD89D' }</div>
            </div>
            <div className='item' id='second'>
              <div id='layout'>{ figures.trapezoid '#F2D169' }</div>
            </div>
            <div className='item' id='third'>
              <div id='layout'>{ figures.trapezoid '#EC9D65' }</div>
            </div>
            <div id='food' className='untouchable'>
              <div id='food_2'>
                <img src='/img/pictures/food_2.png'/>
              </div>
              <div id='food_4'>
                <img src='/img/pictures/food_4.png'/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



export default outfit Admin
