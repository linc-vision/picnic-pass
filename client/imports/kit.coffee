import { SimpleLoading } from '/client/imports/ui/parts/kit'
import { Home } from '/client/imports/ui/parts/kit'
import { Home2 } from '/client/imports/ui/parts/kit'
import { Home3 } from '/client/imports/ui/parts/kit'
import { Admin } from '/client/imports/ui/parts/kit'



App = class extends Component
  constructor: (props) ->
    super props



  render: =>
    <div id='App' className={ classNames 'mobile': IS.mobile(), 'loading': not @props.app.status.loaded }>
      {
        if not @props.app.status.loaded
          <div id='layout'>
            <SimpleLoading/>
          </div>
        else
          <div id='layout'>
            <Switch>
              <Route exact path="/" render={ =>
                if @props.app.config.landing_option is 1
                  <Home/>
                else if @props.app.config.landing_option is 2
                  <Home2/>
                else if @props.app.config.landing_option is 3
                  <Home3/>
              }/>
              <Route path='/admin' render={ => <Admin/> }/>
              <Redirect from='*' to='/'/>
            </Switch>
          </div>
      }
    </div>



export default outfit App
