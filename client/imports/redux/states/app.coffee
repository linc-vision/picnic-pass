origin =
  status:
    user:
      logged: Meteor.userId() isnt null
      sessionLogged: no
      sessionId: ''
      enteredPhoneNumber: no
      referral: ''
      referralUrl: ''
      peopleAhead: 100000
    loaded: no



export default (state = origin, action) ->
  switch action.type

    when 'SET_INIT_CONFIG'
      state = { state... }

      state.config = action.payload.config

      state



    when 'SET_APP_LOADED'
      state = { state... }

      state.status.loaded = yes

      state



    when 'SET_SESSION'
      state = { state... }

      state.status.user = {
        state.status.user...
        action.payload.data...
        sessionLogged: yes
        enteredPhoneNumber: yes
        referralUrl: "#{ Meteor.absoluteUrl() }?ref=#{ action.payload.data.referral }"
      }

      state



    when 'SET_CTA'
      state = { state... }

      state.status.user = {
        state.status.user...
        action.payload...
      }

      state



    when 'FOLLOW_TO_REFERRAL'
      state = { state... }

      state.status.user = {
        state.status.user...
        refFrom: action.payload
      }

      state



    when 'CHECK_LOGIN'
      state = { state... }

      state.status.user = {
        state.status.user...
        logged: Meteor.userId() isnt null
      }

      state



    when 'CHECK_SESSIONS'
      state = { state... }

      state.sessions = action.payload

      state



    else state
