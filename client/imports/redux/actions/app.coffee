export default (transfer, props) ->

  getInitConfig: (data) ->
    Meteor.call 'getInitConfig', data, (err, res) ->
      if err
        console.log err
      else
        transfer
          type: 'SET_INIT_CONFIG'
          payload: res.data.default

        if res.data.follow
          transfer
            type: 'FOLLOW_TO_REFERRAL'
            payload: res.data.follow
        else if res.data.cta
          transfer
            type: 'SET_CTA'
            payload: res.data
        else
          if props.location.search.length > 0
            props.history.push '/'



  setAppLoaded: ->
    transfer
      type: 'SET_APP_LOADED'



  setSession: (data) ->
    transfer
      type: 'SET_SESSION'
      payload: data



  checkLogin: ->
    transfer
      type: 'CHECK_LOGIN'
